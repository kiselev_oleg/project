/*window $*/

window.addEventListener("DOMContentLoaded", function () {
    const off = "finalsupport-tar-off m-auto";
    const on = "finalsupport-tar-on  m-auto";
    const d = document;
    function cl() {
        let t = this;
        d.getElementsByClassName(on)[0].className = off;
        t.className = on;
    }
    d.getElementsByClassName(off)[0].onclick = cl;
    d.getElementsByClassName(off)[1].onclick = cl;
    d.getElementsByClassName(on)[0].onclick = cl;
});
