/*window $*/

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    let el = d.getElementById("FAQ");
    let elm = el.getElementsByTagName("li");
    let f = function () {
        el.getElementsByClassName("FAQAccordionActive")[0].className = "";
        this.className = "FAQAccordionActive";
    };
    let i = 0;
    for (i = 0; i < elm.length; i += 1) {
        elm[i].onclick = f;
    }
});
