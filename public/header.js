/*window $*/

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    let menues = d.getElementsByClassName("mdmenulist");
    let i = 0;
    let bl = function (el) {
        return function () {
            el.style.display = "block";
        };
    };
    let no = function (el) {
        return function () {
            el.style.display = "none";
        };
    };
    for (i = 0; i < menues.length; i += 1) {
        let el = menues[i].getElementsByClassName("mdmenu")[0];
        menues[i].onmouseover = bl(el);
        menues[i].onmouseout = no(el);
    }
});

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    let menues = d.getElementsByClassName("menulist");
    let f1 = function (el) {
        return function () {
            if (el.style.display === "block") {
                el.style.display = "none";
            } else {
                el.style.display = "block";
            }
        };
    };
    let i = 0;
    for (i = 0; i < menues.length; i += 1) {
        let el = menues[i].getElementsByClassName("menuin")[0];
        menues[i].getElementsByTagName("a")[0].onclick = f1(el);
    }
    let el2 = d.getElementById("header-menubutton");
    let t = d.getElementById("menu");
    el2 = el2.getElementsByTagName("img")[0];
    el2.onclick = function () {
        if (t.style.display === "block") {
            t.style.display = "none";
        } else {
            t.style.display = "block";
        }
    };
});
