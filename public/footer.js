/*window $*/

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    d.getElementsByClassName("footer-ckeckbox")[0].onclick = function () {
        let firstChild = this.getElementsByTagName("span")[0];
        if (this.getElementsByTagName("input")[0].checked === true) {
            firstChild.innerHTML = ".";
            this.getElementsByTagName("input")[0].checked = false;
        } else {
            firstChild.innerHTML = "Да";
            this.getElementsByTagName("input")[0].checked = true;
        }
    };
});

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    let button = d.getElementById("footer-form-submit");
    button.onclick = function () {
        let data = {
            name: "",
            numer: "",
            email: "",
            message: "",
            agree: true
        };
        data.name = d.getElementsByName("name")[0].value;
        data.email = d.getElementsByName("email")[0].value;
        data.message = d.getElementsByName("message")[0].value;
        data.numer = d.getElementsByName("numer")[0].value;
        data.agree = Boolean(d.getElementsByName("agree")[0].checked);
        if (!data.agree || data.name === "" || data.email === "") {
            return;
        }
        if (data.numer === "" || data.message === "") {
            return;
        }
        let httpRequest = new XMLHttpRequest();
        httpRequest.open("POST", "https://formcarry.com/s/IvInFE9Z8b");
        httpRequest.setRequestHeader("Content-Type", "application/json");
        httpRequest.setRequestHeader("Accept", "application/json");
        httpRequest.send(JSON.stringify(data));
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                button.style.display = "none";
                let inpend = d.getElementById("footer-form-submitend");
                inpend.style.display = "block";
            }
        };
    };
});
