/*window $*/

window.addEventListener("DOMContentLoaded", function () {
    const d = document;
    let el = d.getElementsByClassName("reviews-display")[0];
    let sp = el.getElementsByTagName("span")[0];
    let Img = d.getElementById("reviews");
    Img = Img.getElementsByTagName("img")[0];
    const max = 8;
    let page = 1;
    el.getElementsByTagName("img")[0].onclick = function () {
        if (page <= 1) {
            return;
        }
        page = page - 1;
        sp.innerHTML = `${page}
            <span style="color: rgba(130,130,137,0.7);">/${max}`;
        Img.src = `images/reviews/${page}.png`;
    };
    el.getElementsByTagName("img")[1].onclick = function () {
        if (page >= 8) {
            return;
        }
        page = page + 1;
        sp.innerHTML = `${page}
            <span style="color: rgba(130,130,137,0.7);">/${max}`;
        Img.src = `images/reviews/${page}.png`;
    };
});
